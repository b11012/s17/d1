// console.log("hi")

// Functions
	// Functions are lines/ block of codes that tell our device/ application to perform a certain task when called or invoked
	// Functions are mostly created to create complicated tasks to run several lines of code in succession
	// They are also used to prevent repeating lines/blocks of codes that perform the same task/function

// Function Declarations
	/*
		function functionName(){
			code block(statements)
		};
		
	*/
	// function keyword - used to defined a javascript functions
	// functionName - the function name. Functions are named to be able to use later in the code.
	// function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.

// a function to print name
	
	function printName(){
		console.log("My name is Tine");
	};

// Function invocation
	//The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements inside a function is executed when the function is invoked or called.
	
	//It is common to use the term "call a function" instead of "invoke a function".
	printName();
	// result: will run the console.log
	printName();
	// result: will run the function again

	// declaredFunction();
	// err: declaredFunction is not defined, you cannot invoke a function unless it is declared


// Function declaration vs. expressions

	//A function can be created through function declaration by using the function keyword and adding a function name.

	//Declared functions are not executed immediately. They are "saved for later use", and will be executed later, when they are invoked (called upon).

	declaredFunction(); //declared functions can be hoisted. As long as the function has been defined.

	function declaredFunction(){
		console.log("Hi, I am from declaredFunction()");
	};

	declaredFunction();

	// Function expressions

	//A function can also be stored in a variable. This is called a function expression.

	//A function expression is an anonymous function assigned to the variableFunction

	// Anonymous Function - a function without a name

	// variableFunction(); // error - function expressions, being stored in a let or const variable, cannot be hoisted.
	

	let variableFunction = function(){
		console.log("Hi I am a variable function");
	};

	variableFunction();

	// We can also create a function expression of a named function.
	// However, to invoke the function expression, we invoke it by its variable name, not by its function name.
	// Function Expressions are always invoked (called) using the variable name.


	let funcExpression = function funcName(){
		console.log("Hello from the other side");
	};

	funcExpression();

	//reassign declared function 

	declaredFunction = function(){
		console.log("This is an updated declaredFunction");
	}

	declaredFunction();

	// reassignment of a function expression with const is not possible

	const constantFunc = function(){
		console.log("Initialized with const");
	};

	constantFunc();

	// constantFunc = function(){
	// 	console.log("Cannot be reassigned");
	// };

	// constantFunc();
	// result: err

// Function scoping
	/*	
		Scope is the accessibility (visibility) of variables within our program.
		
		Javascript Variables has 3 types of scope:
			1. local/block scope
			2. global scope
			3. function scope
	*/	

	{
		let localVar = "Armando Perez"
	};

	let globalVar = "Mr. Worldwide";

	console.log(globalVar);
	// console.log(localVar) // result- not defined

	//Function Scope
	/*		
		JavaScript has function scope: Each function creates a new scope.
		Variables defined inside a function are not accessible (visible) from outside the function.
		Variables declared with var, let and const are quite similar when declared inside a function.
	*/


	function showNames(){

		//Function scoped variables:
		var functionVar = "Joe";
		let functionLet = "Jane";
		const functionConst = "Jake";

		console.log(functionVar);
		console.log(functionLet);
		console.log(functionConst);
	};

	showNames();

	// console.log(functionVar);
	// console.log(functionLet);
	// console.log(functionConst);
	// err: the variables are not defined

	/*
		The variables, functionVar,functionConst and functionLet, are function scoped and cannot be accessed outside of the function they were declared in.
	*/



// Nested Function
	//You can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variable, name, as they are within the same scope/code block.

	function myNewFunction(){
		let name = "Yor";

		function nestedFunction(){
			let nestedName = "Brando";
			console.log(nestedName);
		};
		// console.log(nestedName); //result in err
		nestedFunction();
	};

	myNewFunction();

// Function and Global Scoped Variables
	
	//Global Scoped Variable
	let globalName = "Alex";

	function myNewFunction2(){
		let nameInside = "Marco";

		//Variables declared Globally (outside any function) have Global scope.
		//Global variables can be accessed from anywhere in a Javascript 
		//program including from inside a function.

		console.log(globalName);
	};

	myNewFunction2();
	// console.log(nameInside); /results to an error. 
	//nameInside is function scoped. It cannot be accessed globally.

// alert()
	/*
		Syntax:
		alert("message");
	*/

	alert("I am here");
	//run immediately once the page loads

	function showSampleAlert(){
		alert("Hello User");
	};

	showSampleAlert();

	console.log("I will only log in the console when the alert is dismissed.");

	// prompt()
		/* 
			Syntax:
			prompt("dialogInString")
		*/

		let samplePrompt = prompt("Enter your name");
		console.log(typeof samplePrompt);
		// result: string
		console.log("Hello " + samplePrompt);

		// if there's no input it will return an empty string
		// if prompt was cancelled, the return will be null
	
		function printWelcomeMessage(){
			let firstName = prompt("Enter your first name")
			let lastName = prompt("Enter your last name");
			console.log("Hello "+ firstName + " " + lastName + "!")
			console.log("Welcome to my page");
		};

		printWelcomeMessage();

// Function Naming Convention

	// Function should be definitive of its task, Usually contains a verb

	function getCourses(){

		let courses = ["Science 101", "Trigonometry 103", "Physics 107"]
		console.log(courses);
	};

	getCourses();

	// Avoid generic names

	function get(){
		let name = "Edward";
		console.log(name);
	};
	get();

	// Avoid pointless and inappropriate function names

	function foo(){
		console.log(25%5)
	};

	foo();

	// Name your function in small caps. Follow camelCase when naming variables and function

	function displayCarInfo(){
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1,500,000")
	};

	displayCarInfo();